/*
* @const  initialise l'api
*/
const get_api_users_uri = "https://web-help-request-api.herokuapp.com/users";
const get_api_tickets_uri = "https://web-help-request-api.herokuapp.com/tickets";
let selectUser = document.getElementById("userSelect");
let SelectTask = document.getElementById("ticketsTable")

let arrayUser = []

/*
*  async
*/
async function appendUsers() {
    const rawUsersResponseFromApi = await fetch(get_api_users_uri)
    const jsonifyedUsers = await rawUsersResponseFromApi.json()
    const parsedUsers = jsonifyedUsers.data

    parsedUsers.forEach(user => {
        arrayUser[user.id] = user.username
        selectUser.innerHTML += `<option value="${user.id}">${user.username}</option>`
    })
}

const promesse = new Promise(async (resolve, reject) => {
    await appendUsers()
})
promesse
    .then(() =>
        console.log('wesh')).catch(() => console.error('lis la doc wesh'))

/*
*  function: append get the tickets from the api
*/
async function appendTickets() {
    const rawTicketsFromApi = await fetch(get_api_tickets_uri)

    const parsedTickets = await rawTicketsFromApi.json()
    return parsedTickets.data
}

await appendTickets()
